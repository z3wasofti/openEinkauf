import pathlib
import yaml
import logging


logging.getLogger(__name__)

BASE_DIR = pathlib.Path(__file__).parent
config_path = BASE_DIR / 'config.yml'

def get_config(path=config_path):
    with open(path) as f:
        config = yaml.load(f)
        logging.info(f'Loading config from {config_path}')
    return config
