from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import datetime
import sql_scheme

today = datetime.datetime.now()

class Operations(object):

    def __init__(self, path_to_database):
        self.path = path_to_database
        self.engine = create_engine(self.path, echo=True)
    
    def create_scheme(self, sql_scheme):    
        self.base = sql_scheme
        self.base.metadata.create_all(bind=self.engine)
    
    def attach_session(self):
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()
   
    def add_user(self, name, surname, balance=0):
        self.session.add(sql_scheme.User(name=name, surname=surname, balance=balance))
        self.session.commit()
    
    def add_purchase(self, place, date, payer_id):
        self.session.add(sql_scheme.Purchase(place=place, date=today, payer=payer_id))
        self.session.commit()

    def add_item(self, name, size, messurement):
        self.session.add(sql_scheme.Item(name=name, size=size, messurement=messurement))
        self.session.commit()
    
    def add_brought(self, item_id, quantity, reciver_id):
        self.session.add(sql_scheme.Brought(item=item_id, quantity=quantity, reciver=reciver_id))
        self.session.commit()