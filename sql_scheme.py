from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Sequence, DateTime, ForeignKey
from sqlalchemy.orm import relationship

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, Sequence('users_id_seq'), primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    surname = Column(String(50))
    balance = Column(Integer)

class Purchase(Base):
    __tablename__ = 'purchases'
    id = Column(Integer, Sequence('purchases_id_seq'), primary_key=True)
    place = Column(String(50))
    date = Column(DateTime)
    payer = Column(Integer, ForeignKey('users.id'))

class Item(Base):
    __tablename__ = 'items'
    id = Column(Integer, Sequence('items_id_seq'), primary_key=True)
    name = Column(String(50))
    size = Column(Integer)
    messurement = Column(String(50))

class Brought(Base):
    __tablename__ = 'broughts'
    id = Column(Integer, Sequence('broughts_id_seq'), primary_key=True)
    item = Column(Integer, ForeignKey('items.id'))
    quantity = Column(Integer)
    reciver = Column(Integer, ForeignKey('users.id'))


