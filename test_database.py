#!/usr/bin/python3

import logging
import os
from database_operations import Operations
import sql_scheme
import settings

config = settings.get_config()

logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')

if os.path.exists("database.sqlite3"):
    os.unlink("database.sqlite3")

ops = Operations(config['path']['sqlite'])
#Testing Table Creation
ops.create_scheme(sql_scheme.Base)
ops.attach_session()
ops.add_user("Wayne", "John")
ops.add_purchase("Lidl", '23042019', 1)
ops.add_item("Wasser", "1", "Liter")
ops.add_brought(1, 20, 1)
#Testing Insert

